#!/bin/bash
echo ""
entrada=1
while [ $entrada -eq 1 ];
do
    echo "Digite o endereço de ping desejado: "
    read adress

    bloco_de_notas=$adress-$(date +"%F-%T").txt

    ping $adress -c $var > $bloco_de_notas
    cat $bloco_de_notas

    echo "1. Rodar script novamente."
    echo "2. Sair."
    read entrada2
    while [ $entrada2 -ne 1 -a $entrada2 -ne 2 ];
    do
        echo "O número digitado é inválido!"
        read entrada2
    done
    if [ $entrada2 -eq 1 ];
    then
        entrad=1
    else
        entrada=$entrada2
    fi
done
